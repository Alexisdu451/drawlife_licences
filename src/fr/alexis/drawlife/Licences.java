package fr.alexis.drawlife;

import fr.alexis.drawlife.database.Database;
import fr.alexis.drawlife.gui.LicenceGUI;
import fr.alexis.drawlife.player.LicenceCommand;
import fr.alexis.drawlife.player.PlayerListener;
import fr.alexis.drawlife.player.RemoveLicenceCommand;
import fr.alexis.drawlife.player.TestCommand;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Licences extends JavaPlugin {

    private static Database db;

    public void onEnable(){
        db = new Database("isp.fly-serv.com","c2plugin","c2plugin","pluginmdp123");

        registerEvents(new LicenceGUI(), new PlayerListener());

        getCommand("licence").setExecutor(new LicenceCommand());
        getCommand("test").setExecutor(new TestCommand());
        getCommand("remlicence").setExecutor(new RemoveLicenceCommand());


    }

    public static Database getDB(){
        return db;
    }
    private void registerEvents(Listener... listeners) {
        PluginManager pm = Bukkit.getPluginManager();
        for (Listener l : listeners) {
            pm.registerEvents(l, this);
        }
    }

}
