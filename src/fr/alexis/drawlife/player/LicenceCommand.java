package fr.alexis.drawlife.player;

import fr.alexis.drawlife.database.LicenceManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LicenceCommand implements CommandExecutor {
    LicenceManager licenceManager = new LicenceManager();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;
            if (strings.length == 1) {
                player.sendMessage("Licence de " + Bukkit.getPlayer(strings[0]).getName());
                player.sendMessage("Permis Minage: " + licenceManager.getLicence("Minage", Bukkit.getPlayer(strings[0])));
                player.sendMessage("Permis Voiture: " + licenceManager.getLicence("Voiture", Bukkit.getPlayer(strings[0])));
                player.sendMessage("Permis Camion: " + licenceManager.getLicence("Camion", Bukkit.getPlayer(strings[0])));
                player.sendMessage("Permis Armes: " + licenceManager.getLicence("Armes", Bukkit.getPlayer(strings[0])));
                player.sendMessage("Permis Moto: " + licenceManager.getLicence("Moto", Bukkit.getPlayer(strings[0])));

            }
            if (strings.length == 0) {
                player.sendMessage("Permis Minage: " + licenceManager.getLicence("Minage", player));
                player.sendMessage("Permis Voiture: " + licenceManager.getLicence("Voiture", player));
                player.sendMessage("Permis Camion: " + licenceManager.getLicence("Camion", player));
                player.sendMessage("Permis Armes: " + licenceManager.getLicence("Armes", player));
                player.sendMessage("Permis Moto: " + licenceManager.getLicence("Moto", player));
            }


        return false;
    }
}
