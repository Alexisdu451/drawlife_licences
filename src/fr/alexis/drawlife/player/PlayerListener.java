package fr.alexis.drawlife.player;

import fr.alexis.drawlife.database.LicenceManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {


    @EventHandler
    public void onJoin(PlayerJoinEvent event){

        Player player = event.getPlayer();

        if(!new LicenceManager().isExist(player)){
            new LicenceManager().createAccount(player);
        }
    }
}
