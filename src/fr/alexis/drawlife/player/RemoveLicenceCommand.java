package fr.alexis.drawlife.player;

import fr.alexis.drawlife.database.LicenceManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveLicenceCommand implements CommandExecutor {
    LicenceManager licenceManager = new LicenceManager();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {

        if(sender instanceof Player){
            Player player = (Player) sender;
            Player player2 = Bukkit.getPlayer(strings[0]);
            if(strings.length >= 1){
                licenceManager.remLicence(strings[1], player2);
                player2.sendMessage("§3Gendarmerie §6-> §fUn agent de la fonction public vient de vous enlever la licence §3" + strings[1]);

            }
            else
                player.sendMessage("Erreur");
        }

        return false;
    }
}

