package fr.alexis.drawlife.gui;

import fr.alexis.drawlife.database.LicenceManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class LicenceGUI implements Listener {

    public String inventory_name = "Licences";

    public void openInventory(Player player) {
        Inventory inv = Bukkit.createInventory(null, 6 * 9, inventory_name);
        init(inv);
        player.openInventory(inv);
    }

    private void init(Inventory inv) {
        int[] slot = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
        ItemStack glassb = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);

        for (int i : slot)
            inv.setItem(i, glassb);

        ItemStack minage = new ItemStack(339);
        ItemMeta minagemeta = minage.getItemMeta();
        minagemeta.setDisplayName("Permis Minage");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add("§fCeci est la licence de minage");
        lore.add("§fqui vous permet d'exercer le métier de mineur");
        lore.add("");
        lore.add("§fPrix: §61100");
        minagemeta.setLore(lore);
        minage.setItemMeta(minagemeta);


        ItemStack voiture = new ItemStack(339);
        ItemMeta voituremeta = voiture.getItemMeta();
        voituremeta.setDisplayName("Permis Voiture");
        ArrayList<String> lore_voiture = new ArrayList<String>();
        lore_voiture.add("§fCeci est le permis Voiture");
        lore_voiture.add("§fqui vous permet de conduire une voiture");
        lore_voiture.add("");
        lore_voiture.add("§fPrix: §61500");
        voituremeta.setLore(lore_voiture);
        voiture.setItemMeta(voituremeta);

        ItemStack camion = new ItemStack(339);
        ItemMeta camionmeta = camion.getItemMeta();
        camionmeta.setDisplayName("Permis Poid Lourd");
        ArrayList<String> lore_camion = new ArrayList<String>();
        lore_camion.add("§fCeci est le permis Poid Lourd");
        lore_camion.add("§fqui vous permet de conduire un poid lourd");
        lore_camion.add("");
        lore_camion.add("§fPrix: §62100");
        camionmeta.setLore(lore_camion);
        camion.setItemMeta(camionmeta);

        ItemStack armes = new ItemStack(339);
        ItemMeta armesmeta = armes.getItemMeta();
        armesmeta.setDisplayName("Permis Armes");
        ArrayList<String> lore_armes = new ArrayList<String>();
        lore_armes.add("§fCeci est la licence d'arme");
        lore_armes.add("§fqui vous permet de posseder une arme");
        lore_armes.add("");
        lore_armes.add("§fPrix: §63500");
        armesmeta.setLore(lore_armes);
        armes.setItemMeta(armesmeta);

        ItemStack moto = new ItemStack(339);
        ItemMeta motometa = moto.getItemMeta();
        motometa.setDisplayName("Permis Moto");
        ArrayList<String> lore_moto = new ArrayList<String>();
        lore_moto.add("§fCeci est le permis moto");
        lore_moto.add("§fqui vous permet de conduire une moto");
        lore_moto.add("");
        lore_moto.add("§fPrix: §6600");
        motometa.setLore(lore_moto);
        moto.setItemMeta(motometa);

        inv.setItem(20, minage);
        inv.setItem(22, voiture);
        inv.setItem(24, camion);
        inv.setItem(29, armes);
        inv.setItem(31, moto);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getClick() == null)
            return;
        if (e.getInventory().getName().equals(inventory_name)) {
            e.setCancelled(true);
            if (e.getSlot() == 20) {
                p.closeInventory();
                new LicenceManager().checkMoney("Minage", p, 1100);
            }
            if (e.getSlot() == 22) {
                p.closeInventory();
                new LicenceManager().checkMoney("Voiture", p, 1500);
            }

            if (e.getSlot() == 24) {
                p.closeInventory();
                new LicenceManager().checkMoney("Camion", p, 2100);
            }

            if (e.getSlot() == 29) {
                p.closeInventory();
                new LicenceManager().checkMoney("Armes", p, 3500);
            }

            if (e.getSlot() == 21) {
                p.closeInventory();
                new LicenceManager().checkMoney("Moto", p, 600);
            }



        }
    }
}
