package fr.alexis.drawlife.database;

import fr.alexis.drawlife.Licences;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import static org.bukkit.Bukkit.getServer;

public class LicenceManager {

    public static Economy economy = null;

    public String getLicence(String licence_name, Player player) {
        return (String) Licences.getDB().read("SELECT * FROM players_licences WHERE player_name='" + player.getName() + "'", licence_name);
    }

    public void setLicence(String licence_name, Player player) {
        Licences.getDB().update("UPDATE players_licences SET " + licence_name + "='Oui' WHERE player_name='" + player.getName() + "'");
        player.sendMessage("§3Préfecture §6-> §fVous venez d'acheter la licence " + licence_name);
    }

    public void remLicence(String licence_name, Player player) {
        Licences.getDB().update("UPDATE players_licences SET " + licence_name + "='Non' WHERE player_name='" + player.getName() + "'");
        player.sendMessage("§3Gendarmerie §6-> §fVous venez de retirer la licence  de §3" + licence_name + " §fà §3" + player.getName());
    }

    public void createAccount(Player player) {
        Licences.getDB().update("INSERT INTO players_licences (player_name) VALUES ('" + player.getName() + "')");
    }

    public void checkLicence(String licence_name, Player player) {
        if (getLicence(licence_name, player).equals("Non"))
            setLicence(licence_name, player);
        else
            player.sendMessage("§3Préfecture §6-> §fVous possedez déjà la  licence §3" + licence_name);
    }

    public boolean isExist(Player player) {

        ResultSet rs = Licences.getDB().getResult("SELECT * FROM players_licences WHERE player_name='" + player.getName() + "'");
        try {
            return rs.next();
        } catch (SQLException e) {
            getServer().getLogger().log(Level.WARNING, e.getMessage(), e);
        }

        return false;
    }

    public void checkMoney(String licence_name, Player player, int prix) {

        if (setupEconomy()) {

            double balance = economy.getBalance(player);

            if (prix <= balance) {
                checkLicence(licence_name, player);
                economy.withdrawPlayer(player, prix);
            } else
                player.sendMessage("§3Préfecture §6-> §fVous n'avez pas assez d'argent pour acheter laz licence §3" + licence_name);
        }
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

}
